// Set this to be your projects breakpoints
const sizes = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
}

const isAbove = {}

Object.entries(sizes).forEach(([size, val]) => {
  isAbove[size] = window.matchMedia(`(min-width: ${val}px)`)
})

/* You can use this instead of window.resize events in a lot of cases (MUCH more performant)

Replace "breakpoint" in the below examples with any of the sizes in the sizes object above

// Single use (e.g on page load or to only trigger once)
if (isAbove.breakpoint.matches) {
  // do stuff when browser is breakpoint size or larger.
  Note: you can do the reverse (e.g when something is tablet or SMALLER by !isAbove.breakpoint.matches)
}

// Multiple use, everytime a breakpoint is passed
Or isAbove.breakpoint.addListener(e => {
  if (e.matches) {
    This section means it's breakpoint or larger
  } else {
    this section means it's smaller
  }
})

*/

// Note, you only need to import "isAbove", I'm only exporting sizes incase there is ever a need to 
// reference or output the list of sizes... Or hopefully in the future, change the sizes in WP options

export { sizes, isAbove }
