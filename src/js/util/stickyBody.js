import { debounce } from './debounce';

/**
 * Make an element sticky if it scrolls past a specific anchor
 * @param  {any} anchor The anchor (or height) you want to scroll past before el sticks, string '.element' to find it or pass element directly. If it's a number it will scroll past that height.
 * @return {Event Listener} The total of the two numbers
 */

export default async function (anchor, delay = 0) {
  const body = document.body
  const stickyClass = 'sticky';

  let anchorHeight;

  // Allow a string to find, or an element directly... Or even a number for a height
  switch (typeof anchor) {
    case 'string':
      // Find by string
      anchor = document.querySelector(anchor);
      // Stick element when scrolled past elements height
      if (!anchor) {
        return
      }
      anchorHeight = anchor.offsetHeight
      break;
    case 'number':
      // Stick element when scrolled past a set pixel height
      anchorHeight = anchor
      break;
    case 'object': // But really Array, Arrays get passed as typeof "object" :3
      // Loop through array and find the first matching element
      for (let i = 0; i < anchor.length; i++) {
        if (typeof anchor[i] === 'object') {
          anchorHeight = 0
          for (let ii = 0; ii < anchor[i].length; ii++) {
            let findEl = document.querySelector(anchor[i][ii]);
            if (!findEl) {
              break
            }
            anchorHeight += parseInt(findEl.offsetHeight)
          }
        } else {
          let findEl = document.querySelector(anchor[i]);
          if (!findEl) {
            return
          }
          anchorHeight = findEl.offsetHeight
          break;
        }
      }
      break;
    default:
      // Element was passed directly
      // Strick element when scrolled past elements height
      if (!anchor) {
        return console.log(anchor + ' not found')
      }
      anchorHeight = anchor.offsetHeight
  }
  const handleStick = () => {
    console.log({ anchorHeight })
    if (window.scrollY > anchorHeight) {
      if (!body.classList.contains(stickyClass)) {
        body.classList.add(stickyClass)
        window.dispatchEvent(new CustomEvent(`body-stickied`));
      }
    } else {
      if (body.classList.contains(stickyClass)) {
        body.classList.remove(stickyClass)
        window.dispatchEvent(new CustomEvent(`body-unstickied`));
      }
    }
  }


  window.addEventListener('scroll', debounce(handleStick, 250));
}
