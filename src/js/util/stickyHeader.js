import debounce from '../util/debounce'

export default async function () {
  const body = document.querySelector('body')
  const header = document.getElementById('main-header')
  const stickyClass = 'sticky';

  let anchorHeight = header.clientHeight;

  function handleStick() {
    if (window.scrollY > anchorHeight) {
      if (!body.classList.contains(stickyClass)) {
        body.classList.add(stickyClass)
        window.dispatchEvent(new CustomEvent(`body-stickied`));
      }
    } else {
      if (body.classList.contains(stickyClass)) {
        body.classList.remove(stickyClass)
        window.dispatchEvent(new CustomEvent(`body-unstickied`));
      }
    }
  }
  handleStick()
  return window.addEventListener('scroll', debounce(() => handleStick(), 50, false, true));
}
