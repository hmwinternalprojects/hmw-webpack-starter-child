import "core-js/stable";
import "regenerator-runtime/runtime";

// import Vue from 'vue';

// Auto Find all vue components
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => {
//   let name = key
//     .split('/')
//     .pop()
//     .split('.')[0];
//   Vue.component(name, files(key).default);
// });


// Control which page JS fires on with this router
import Router from './util/Router';

// Import each page / type here
import common from './routes/common';
import woocommerce from './routes/woocommerce';

/** Populate Router instance with DOM routes */
const routes = new Router({
  common,
  woocommerce
});

// Load Events
window.onload = () => {
  routes.loadEvents()
}

// let app = new Vue({
//   el: '#page'
// });
