import { isAbove } from '../util/breakpoints';
import { debounce } from '../util/debounce';

export default function (options = {}) {
  let body = document.body,
    mainNav = document.getElementById('main-nav'),
    mainHeader = document.getElementById('main-header'),
    mobileToggle = document.getElementById('menu-toggle'),
    alawysShow = mainNav.classList.contains('always_show_mobile_nav') || false,
    mobileMenuActive = false,
    collapsible = false,
    originalHrefs = [],
    headerElements = [],
    mainNavParent,
    mobileMenu,
    headerElementPositions;

  options = {
    clone_items: true,
    always_show: alawysShow,
    cloned_text_prefix: null,
    cloned_text_suffix: null,
    move_to_nav: null,
    breakpoint: 'lg',
    enableCollapsible: true,
    ...options
  }

  if (options.move_to_nav) {
    // If any move to nav items exist, map the array names to the actual DOM elements
    headerElements = options.move_to_nav.map(el => document.querySelector(el))

    // Intiialize a new map to store data about each header element position in the DOM
    headerElementPositions = new Map();
  }

  // Hide toggle 
  if (!options.always_show) {
    mobileToggle.style.display = 'none';
  } else {
    mobileToggle.style.display = 'block';
  }

  if (!mainNav) {
    return console.log('For responsive menu to work, your main nav needs the ID: %c#main-nav', 'color: red')
  }

  if (options.enableCollapsible) {
    setup_collapsible_submenus()
  }

  if (!isAbove[options.breakpoint].matches && !mobileMenuActive || options.always_show) {
    setupMobileMenu()
  }

  resizeCheck(isAbove[options.breakpoint])


  isAbove[options.breakpoint].addListener(e => {
    resizeCheck(e)
  })

  function resizeCheck(e) {
    if (e.matches) {
      if (mobileMenuActive && !options.always_show) {
        destroyMobileMenu()
      }
      if (options.always_show && headerElements) {
        showHeaderElements()
      }
    } else {
      if (!mobileMenuActive) {
        setupMobileMenu()
      }
      if (options.always_show && headerElements) {
        hideHeaderElements()
      }
    }
  }

  function setupMobileMenu() {
    mobileMenuActive = true;

    // Show toggle 
    if (!options.always_show) {
      mobileToggle.style.display = 'block';
    }

    body.classList.add('has-mobile-menu')

    if (!options.always_show) {
      mobileMenu = document.createElement('div')
      mobileMenu.classList.add('mobile-menu');

      // Append mobile-menu to page
      body.append(mobileMenu)

      // Remember original positions
      mainNavParent = mainNav.parentNode

      if (headerElements) {
        hideHeaderElements()
      }

      // Move to mobile nav
      document.querySelector('.mobile-menu').prepend(mainNav)

    } else {
      mobileMenu = document.querySelector('.mobile-menu');
    }

    mobileToggle.addEventListener('click', toggleMenuVisibility, true)

    if (!collapsible) {
      setup_collapsible_submenus();
    }
  }

  function closeOnOutsideClick(e) {
    let target = e.target;
    if (!mobileMenu.contains(target)) {
      toggleMenuVisibility(e)
    }
  }

  function toggleMenuVisibility(e) {
    e.preventDefault();
    e.stopPropagation();
    if (body.classList.contains('mobile-menu-is-visible')) {
      body.classList.remove('mobile-menu-is-visible');
      mobileMenu.setAttribute('aria-expanded', 'false');
      document.removeEventListener('click', closeOnOutsideClick, true)
    } else {
      body.classList.add('mobile-menu-is-visible');
      mobileMenu.setAttribute('aria-expanded', 'true');
      document.addEventListener('click', closeOnOutsideClick, true)
    }
  }


  function hideHeaderElements() {
    // Create a clone of the header BEFORE anything is hidden, this keeps the indexes in tact
    let headerClone = mainHeader.querySelector('.container').cloneNode(true)

    headerElements.forEach(el => {

      let parent = el.parentNode;

      // This will find the index of where the element is inside it's parent
      let index = [...headerClone.children].findIndex(arrayEl => {
        return arrayEl.isEqualNode(el)
      })

      // Account for mainNav moving if enabled
      if (!options.always_show) {
        index++
      }

      if (!index) {
        throw new Error(`Index was not found for Element ${el}`)
      }

      // Only need the parent and the index to find it's original location
      let data = {
        parent,
        index
      }

      // Save the position data to our map if we don't aready have it there
      if (!headerElementPositions.get(el)) {
        headerElementPositions.set(el, data);
      }

      // Move everything into the mobile-menu div (after main nav)
      mobileMenu.appendChild(el)

    })

    // Cleanup
    headerClone.remove();

  }

  function showHeaderElements() {
    headerElements.forEach(el => {
      let data = headerElementPositions.get(el)

      if (data) {
        let parent = data.parent;
        parent.insertBefore(el, parent.children[data.index]);
      }
    })
    // Clear the map -- If we find we need the map to be generated each time, rather than "caching it"
    // headerElementPositions.clear();
  }

  function destroyMobileMenu() {
    mobileMenuActive = false;

    // Hide toggle 
    if (!options.always_show) {
      mobileToggle.style.display = 'none';
    }

    mobileToggle.removeEventListener('click', toggleMenuVisibility, true)

    body.classList.remove('has-mobile-menu')
    body.classList.remove('mobile-menu-is-visible')

    mainNavParent.append(mainNav)

    if (headerElements) {
      showHeaderElements()
    }

    mobileMenu.remove();

    if (collapsible && !options.enableCollapsible) {
      remove_collapsible_submenus();
    }
  }

  function toggleSubMenu(e) {
    e.stopPropagation()
    let el = e.target
    let subMenu = el.parentNode.querySelector(':scope > .sub-menu')
    if (subMenu) {
      e.preventDefault()
      if (el.getAttribute('href') !== '#') {

        el.setAttribute('href', '#');
      }
      el.parentNode.classList.toggle('is-open')
      subMenu.classList.toggle('is-visible')
    }
  }

  // This will allow tapping to open nested menus -- Only way for mobile I could think of
  function setup_collapsible_submenus() {
    mainNav.querySelectorAll('.menu-item-has-children > a').forEach(el => {
      originalHrefs.push({
        el,
        url: el.getAttribute('href'),
      })

      if (options.enableCollapsible) {
        el.parentNode.classList.add('collapsible-enabled');
      }

      let slug = el.innerText.toLowerCase().replaceAll(' ', '-'),
        prefixText = options.cloned_text_prefix,
        suffixText = options.cloned_text_suffix;

      // Duplicate top-level into sub-menu
      if (options.clone_items) {
        let a = el.cloneNode(true);


        if (prefixText) {
          if (typeof prefixText === 'object') {
            if (prefixText[slug]) {
              a.innerText = `${prefixText[slug]}${a.innerText}`
            } else if (prefixText['default']) {
              a.innerText = `${a.innerText}${prefixText['default']}`
            }
          } else {
            a.innerText = `${prefixText}${a.innerText}`
          }
        }

        if (suffixText) {
          if (typeof suffixText === 'object') {
            if (suffixText[slug]) {
              a.innerText = `${a.innerText}${suffixText[slug]}`
            } else if (suffixText['default']) {
              a.innerText = `${a.innerText}${suffixText['default']}`
            }
          } else {
            a.innerText = `${a.innerText}${suffixText}`
          }
        }

        let li = document.createElement('li');
        // let classes = [...el.parentNode.classList].filter(cls => cls !== 'menu-item-has-children' && cls !== 'focus')
        li.classList.add('menu-item', 'cloned-item');
        li.appendChild(a)

        el.parentNode.querySelector(':scope > .sub-menu').prepend(li)
      }
    })
    mainNav.addEventListener('click', toggleSubMenu, true)

    collapsible = true;
  }

  // Reset collapsible states back to normal (so regular clicks work with hover state)
  function remove_collapsible_submenus() {
    // Remove any items that might still be open
    let isVisible = document.querySelectorAll('.is-visible');
    isVisible.forEach(el => {
      el.classList.remove('is-visible')
    })

    mainNav.querySelectorAll('.cloned-item').forEach(el => {
      el.remove();
    })

    // Set links back to what they were
    originalHrefs.forEach(originalHref => {
      originalHref.el.setAttribute('href', originalHref.url)
    })

    // Clean up event listener
    mainNav.removeEventListener('click', toggleSubMenu, true)
    // mainNav.removeEventListener('click', toggleSubMenu, false)

    collapsible = false;
  }

  // Toggle Focus on every link desktop and mobile
  (function () {
    // Get all the link elements within the menu.
    let links = mainNav.getElementsByTagName('a');

    if (!links || !links.length) { return }

    // Each time a menu link is focused or blurred, toggle focus.
    for (let i = 0, len = links.length; i < len; i++) {
      links[i].addEventListener('focus', toggleFocus, true);
      links[i].addEventListener('blur', toggleFocus, true);
    }

    /**
     * Sets or removes .focus class on an element.
     */
    function toggleFocus() {
      var self = this;

      // Move up through the ancestors of the current link until we hit .main-menu-nav.
      while (-1 === self.className.indexOf('main-menu-nav')) {
        // On li elements toggle the class .focus.
        if ('li' === self.tagName.toLowerCase()) {
          if (-1 !== self.className.indexOf('focus')) {
            self.className = self.className.replace(' focus', '');
          } else {
            self.className += ' focus';
          }
        }

        self = self.parentElement;
      }
    }


    /**
     * Toggles `focus` class to allow submenu access on tablets.
     */
    (function (container) {
      var touchStartFn,
        i,
        parentLink = container.querySelectorAll('.menu-item-has-children > a, .page_item_has_children > a');

      if ('ontouchstart' in window && parentLink.length) {
        touchStartFn = function (e) {
          var menuItem = this.parentNode,
            i;

          if (!menuItem.classList.contains('focus') && menuItem.classList.contains('is-open')) {
            e.preventDefault();
            for (i = 0; i < menuItem.parentNode.children.length; ++i) {
              if (menuItem === menuItem.parentNode.children[i]) {
                continue;
              }
              menuItem.parentNode.children[i].classList.remove('focus');
            }
            menuItem.classList.add('focus');
            menuItem.click()
          } else {
            menuItem.classList.remove('focus');
          }
        };

        for (i = 0; i < parentLink.length; ++i) {
          parentLink[i].addEventListener('touchstart', touchStartFn, false);
        }
      }
    })(mainNav);
  })()

  /* The following code is only responsible for making the nav go up and down based on scroll direction */
  const supportPageOffset = window.pageXOffset !== undefined;
  const isCSS1Compat = (document.compatMode || "") === "CSS1Compat";

  let previousScrollPosition = 0;

  const isScrollingDown = () => {
    // Get the current scroll position, a few checks to support IE if 100% needed.... -_-
    let scrolledPosition = supportPageOffset
      ? window.pageYOffset
      : isCSS1Compat
        ? document.documentElement.scrollTop
        : document.body.scrollTop;
    let isScrollDown;

    // Check if the current position is greater than the last known position
    if (scrolledPosition > (previousScrollPosition + 10)) {
      isScrollDown = true;
    } else {
      isScrollDown = false;
    }

    // Set current position as the new last known position
    previousScrollPosition = scrolledPosition;
    return isScrollDown;
  };

  const toggleScrollClasses = (bool) => {
    if (bool) {
      mainHeader.classList.add("scroll-down");
      mainHeader.classList.remove("scroll-up");
    } else {
      mainHeader.classList.add("scroll-up");
      mainHeader.classList.remove("scroll-down");
    }
  }

  const handleNavScroll = () => {
    // Make sure we are at least past the height of header before doing anything
    if (window.scrollY > mainHeader.clientHeight) {
      // This makes sure you don't currently have focus on any elements within the header. E.g if you have the nav open, it will not scroll up on you.
      if (isScrollingDown() && !mainHeader.contains(document.activeElement)) {
        toggleScrollClasses(true)
      } else {
        toggleScrollClasses(false)
      }
    } else {
      // Show header, we are below height
      toggleScrollClasses(false)
    }
  };

  // Accessibility check, some people don't like shit moving around
  const reducedMotionCheck = window.matchMedia("(prefers-reduced-motion: reduce)");

  if (reducedMotionCheck && !reducedMotionCheck.matches) {
    window.addEventListener("scroll", debounce(handleNavScroll, 250, false));
  }
}
