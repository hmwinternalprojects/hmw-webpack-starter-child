
export default function () {
  let loadMoreBtn = document.querySelector('.loadmore-button');

  if (loadMoreBtn) {
    let queryVars = loadMoreBtn.getAttribute('data-queryvars'),
      currentPage = loadMoreBtn.getAttribute('data-currentpage'),
      offset = loadMoreBtn.getAttribute('data-offset'),
      totalPages = parseInt(loadMoreBtn.getAttribute('data-totalpages')),
      template_part = loadMoreBtn.getAttribute('data-templatepart') || null,
      trigger = loadMoreBtn.getAttribute('data-trigger') || 'click',
      targetContainer = loadMoreBtn.parentNode.querySelector(loadMoreBtn.getAttribute('data-container')),
      spinner = loadMoreBtn.nextElementSibling,
      finished = false,
      loading = false;

    const fetchPosts = () => {
      toggleLoading(true);

      let data = {
        query: queryVars,
        page: currentPage,
        offset,
        template_part
      }


      if (currentPage < totalPages) {
        fetch(`${global_vars.rest_base}hmw_ajax_load_more`, {
          method: "post",
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        })
          .then(res => res.text())
          .then(data => {

            loadMoreBtn.setAttribute('data-currentpage', currentPage++)

            // return console.log(JSON.parse(data))
            targetContainer.innerHTML += data

            // Re-trigger fades
            // enableFadeIn()

            toggleLoading(false);

            // Dispatch this after every load for other JS to tap into
            window.dispatchEvent(new Event('ajax-load-more:loaded'))

            if (currentPage >= totalPages) {
              spinner.remove()
              loadMoreBtn.remove()
              finished = true
            }

          }).catch(err => {
            loadMoreBtn.remove()
            console.log(err)
          })
      }
    }

    const toggleLoading = (bool) => {
      loading = bool
      if (bool) {
        spinner.style.display = 'block'
        loadMoreBtn.classList.add('is-loading')
      } else {
        spinner.style.display = 'none'
        loadMoreBtn.classList.remove('is-loading')
      }
    }

    if (trigger === 'click') {
      loadMoreBtn.addEventListener('click', function (e) {
        e.preventDefault()
        fetchPosts()
      });
    }
    if (trigger === 'scroll') {
      loadMoreBtn.remove();
      const observer = new IntersectionObserver(entries => {
        const firstEntry = entries[0];
        if (firstEntry.isIntersecting) {
          const bcr = firstEntry.boundingClientRect;
          const isBottomVisible = (bcr.bottom < window.innerHeight) && bcr.bottom;
          if (isBottomVisible) {
            if (!finished && !loading) {
              fetchPosts()
            } else {
              observer.unobserve(firstEntry.target)
            }
          }
        }
      }, {
        threshold: Array(11).fill().map((_, i) => i * .1)
      });
      observer.observe(targetContainer)
    }
  }
}
