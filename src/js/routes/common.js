import stickyHeader from '../util/stickyHeader'
import mainMenu from '../components/mainMenu'
import sitewideMessage from '../components/sitewideMessage'
import loadmore from '../components/ajaxLoadMore'

// import { enableFadeIn } from '../components/globalAnimations'

export default {
  init() {
    mainMenu({
      clone_items: true, // Whether or not to add a clone of the parent into the sub menu (so you can still get to that page), disable if you plan to do this manually in WP
      move_to_nav: [".header-cta-block", ".company_phone"], // an array of elements to move [".header-buttons", ".header-phone"]
      breakpoint: 'xl', // What breakpoint you'd like this to break at, you can adjust or add more in breakpoints.js
      enableCollapsible: true, // If you want to use click to open instead of hover across all devices (default is true now)
      cloned_text_prefix: "All ", // By providing a string, this will apply to all cloned items
      cloned_text_suffix: {
        "test-1": " Overview", // By using an object with slug > string you can change the prefix/suffix for each menu item individually
        "default": " default" // If you add a default it will add this to all cloned items that don't already have a specific slug set omitting this will only add to slug items
      }
    });

    loadmore();

    // If sitewide message is found intialise it and use the sticky logic from it
    let sitewideMessageEL = document.querySelector('.sitewide-message')
    if (sitewideMessageEL) {
      sitewideMessage(sitewideMessageEL)
    } else {
      // Otherwise use the sticky logic from this
      stickyHeader();
    }

    // Enable fade animations
    // enableFadeIn();

  },
  finalize() {

  },
};
