const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')


module.exports = {
  context: __dirname,
  entry: {
    frontend: ['./src/js/main.js', './src/sass/style.scss'],
    admin: ['./src/admin/style.scss'],
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name]-bundle.js'
  },
  mode: 'development',
  devtool: 'eval-nosources-cheap-module-source-map',
  resolve: {
    alias: {
      // Vue Runtime Compiler
      'vue$': 'vue/dist/vue.esm.js',
    }
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        exclude: /node_modules/,
        test: /\.jsx$/,
        loader: 'eslint-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader'
      },
      {
        test: /\.s?css$/,
        use: [{
          loader: MiniCssExtractPlugin.loader,
          options: {
            publicPath: ''
          }
        }, {
          loader: 'css-loader',
          options: {
            sourceMap: false,
            url: (url, resourcePath) => {
              // Don't handle relative paths that include /wp-content
              if (url.includes("wp-content")) {
                return false;
              }
              return true;
            },
          }
        }, 'sass-loader', {
          loader: 'sass-resources-loader',
          options: {
            resources: [
              path.resolve(__dirname, "./src/sass/variables-site/_colors.scss"),
              path.resolve(__dirname, "./src/sass/variables-site/_structure.scss"),
              path.resolve(__dirname, "./src/sass/mixins/_breakpoints.scss")
            ],
          }
        }, 'postcss-loader']
        //    {
        //   loader: 'postcss-loader', options: {
        //     ident: 'postcss',
        //     plugins: () => [
        //       postcssEnvFunction({
        //         importFrom: [
        //           './css-env-variables.js',
        //         ]
        //       })
        //     ]
        //   }
        // }
      },
      {
        test: /\.(jpe?g|png|gif)\$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'images/',
              name: '[name].[ext]'
            }
          },
          'img-loader'
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    //new StyleLintPlugin(),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
    new BrowserSyncPlugin({
      files: '**/*.php',
      proxy: 'http://buildy-fresh.local',
    })
  ],
  optimization: {
    minimizer: [new UglifyJsPlugin(), new OptimizeCssAssetsPlugin()]
  }
};
