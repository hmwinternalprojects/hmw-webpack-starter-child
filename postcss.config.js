module.exports = {
  parser: "postcss-scss",
  plugins: {
    autoprefixer: {
      grid: true
    }
  }
}
