<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**** Register custom image sizes *** */
// Gallery Sizes
add_image_size('slider', 1250, 600, true );
add_image_size('masonry', 1250, 300, true );

// Load text domain
load_child_theme_textdomain( 'webpack-starter-child', get_stylesheet_directory() . '/languages' );



function hmw_remove_parent_stuff() {
    wp_dequeue_style( 'hmw-frontend-styles' );
    wp_deregister_style( 'hmw-frontend-styles' );
    wp_dequeue_script( 'hmw-frontend-scripts' );
    wp_deregister_script( 'hmw-frontend-scripts' );
}

add_action( 'wp_enqueue_scripts', 'hmw_remove_parent_stuff', 20 );

function theme_enqueue_styles() {
  // Enqueue Fonts (if any)
  options_google_font_enqueue();

  // Get the theme data
	// $the_theme = wp_get_theme();
  wp_register_script( 'hmw-child-frontend-scripts', get_stylesheet_directory_uri() . '/public/frontend-bundle.js', array('jquery'), null, true );
	wp_localize_script( 'hmw-child-frontend-scripts', 'global_vars', array(
		'admin_ajax_url' => admin_url( 'admin-ajax.php' ),
		'rest_base' => site_url() . '/wp-json/wp/v2/',
  ) );
  
  // Any polyfills for IE11 that are needed here
  // wp_enqueue_script('ie-pollyfil', 'https://polyfill.io/v3/polyfill.min.js?features=IntersectionObserver%2CIntersectionObserverEntry%2CCustomEvent', null, null, false);
  
	// Loads bundled frontend CSS.
  wp_enqueue_style( 'hmw-child-frontend-styles', get_stylesheet_directory_uri() . '/public/frontend.css' );
	wp_enqueue_style( 'hmw-theme-options', get_stylesheet_directory_uri() . '/public/hmw-theme-options.css' );	
  	
  wp_enqueue_script('hmw-child-frontend-scripts');
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

/****************************************************************************************************************
* If any of these files don't exist it will revert to parent themes version
*/
include get_theme_file_path( '/inc/shortcodes/default-shortcodes.php' ); // These are the default shortcodes we bundle 
include get_theme_file_path( '/inc/shortcodes/post-shortcodes.php' ); // Everything related to posts
include get_theme_file_path( '/inc/shortcodes/page-shortcodes.php' ); // Everything related to pages

/**
 * Get all the registered image sizes along with their dimensions
 *
 * @global array $_wp_additional_image_sizes
 *
 * @link http://core.trac.wordpress.org/ticket/18947 Reference ticket
 * @return array $image_sizes The image sizes
 */
function _get_all_image_sizes() {
	global $_wp_additional_image_sizes;

	$default_image_sizes = array( 'thumbnail', 'medium', 'large' );
	 
	foreach ( $default_image_sizes as $size ) {
		$image_sizes[$size]['width']	= intval( get_option( "{$size}_size_w") );
		$image_sizes[$size]['height'] = intval( get_option( "{$size}_size_h") );
		$image_sizes[$size]['crop']	= get_option( "{$size}_crop" ) ? get_option( "{$size}_crop" ) : false;
	}
	
	if ( isset( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) )
		$image_sizes = array_merge( $image_sizes, $_wp_additional_image_sizes );
		
	return $image_sizes;
}

function disable_message_load_field( $field ) {
  $field['readonly'] = 1;
    return $field;
  }
add_filter('acf/load_field/name=sub_field_1', 'disable_message_load_field');
add_filter('acf/load_field/name=sub_field_2', 'disable_message_load_field');


// Add header contact buttons 
add_action( 'main-header:before-nav', function() {
  ?>
  <div class="header-cta-block mr-1 flex items-center col-gap-3">
    <a href="#" class="btn btn--secondary">Book Now</a>
  </div>
  <?php
});

add_action( 'main-header:after-nav', function() {
  echo apply_shortcodes('[company-phone icon="fas fa-phone"]');
});
