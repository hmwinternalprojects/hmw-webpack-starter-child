  <div class="col-lg-4">
    <?php echo do_shortcode('[searchandfilter id="190"]'); ?>
  </div>

  <?php if ( function_exists('woocommerce_product_loop') ) {
  
  /**
   * Hook: woocommerce_before_shop_loop.
   *
   * @hooked woocommerce_output_all_notices - 10
   * @hooked woocommerce_result_count - 20
     * @hooked woocommerce_catalog_ordering - 30
     */
    echo '<div class="col-lg-8">';
    do_action( 'woocommerce_before_shop_loop' );
    
    woocommerce_product_loop_start();

    
    if ( wc_get_loop_prop( 'total' ) ) {
      while ( have_posts() ) {
        the_post();
        
        /**
         * Hook: woocommerce_shop_loop.
         */
        do_action( 'woocommerce_shop_loop' );
        
        wc_get_template_part( 'content', 'product' );
      }
    }

    
    woocommerce_product_loop_end();
    
    /**
     * Hook: woocommerce_after_shop_loop.
     *
     * @hooked woocommerce_pagination - 10
     */
    do_action( 'woocommerce_after_shop_loop' );
    
    // Echo the content inside the page
    $shop_page = get_page(get_option( 'woocommerce_shop_page_id' ));
    echo apply_filters( 'the_content', $shop_page->post_content );

    echo '</div>';
  } else {
    /**
     * Hook: woocommerce_no_products_found.
     *
     * @hooked wc_no_products_found - 10
     */
    do_action( 'woocommerce_no_products_found' );
  }
  
  /**
   * Hook: woocommerce_after_main_content.
   *
   * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
   */
  do_action( 'woocommerce_after_main_content' );

?>